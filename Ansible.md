# Ansible

## O que é o Ansible: 
Ferramenta opensource, desenvolvido por Michael DeHann (Red Hat), que lhe possibilita realizar comunicação e integração com diversos destinos de forma automatizada.

O Ansible trabalha em quatro vertentes:
- Gerenciamento de mudanças:
    - Idempotente: Garante que a tarefa é executada apenas uma única vez através do system state.
    - System State: Estado da máquina.
    - Versão de aplicações: Permite criação de rotinas para utilizar uma versão de um determinado sistema. Caso alguém altere uma versão o ansible consegue reverter as alterações e garantir a versão correta.
    - Alerta de mudanças.

- Provisionamento:
    - Configuração.
    - Instalação.
    - Preparação.
    - Alteração do System State.

- Automação:
    - Execução de tarefas de forma automática.
    - Ordenação de tarefas.
    - Realizar decisões.

- Orquestração:
    - Múltiplos servidores
    - Múltiplas aplicações
    - Diferentes tarefas
    - Ambiente híbrido


## Como funciona?

![Ansible schema](./ansible-schema.png)

## Configuração do ambiente

Afim de facilitar a comunicação entre as máquinas iremos criar uma chave ssh na máquina principal e copiaremos a chave pública para as máquinas targets.

Para criação da chave e copia-lo para a máquina de destino, execute os comandos abaixo:

```sh
ssh-keygen -t rsa -b 4096
ssh-copy-id -i ~/.ssh/id_rsa.pub root@192.168.33.12
```

## Instalação
```sh
sudo apt-add-repository ppa:ansible/ansible -y
sudo apt-get update && sudo apt-get install ansible -y
```

## Adhoc

### O que são comandos adhoc?

Comandos utilizados para uma única finalidade.

Estrutura dos comandos adhoc:
- ansible host -m [modulo] 

```sh
ansible -i hosts all -a "/sbin/reboot" --user root
ansible -i hosts all -m copy -a "src=/etc/hosts dest=/tmp/hosts"
ansible -i hosts all -m systemd -a "name=ssh state=stopped"
ansible -i hosts all -m setup
```

## Modules

* [All modules](https://docs.ansible.com/ansible/2.9/modules/list_of_all_modules.html)
* [Cloud modules](https://docs.ansible.com/ansible/2.9/modules/list_of_cloud_modules.html)
* [Clustering modules](https://docs.ansible.com/ansible/2.9/modules/list_of_clustering_modules.html)
* [Commands modules](https://docs.ansible.com/ansible/2.9/modules/list_of_commands_modules.html)
* [Crypto modules](https://docs.ansible.com/ansible/2.9/modules/list_of_crypto_modules.html)
* [Database modules](https://docs.ansible.com/ansible/2.9/modules/list_of_database_modules.html)
* [Files modules](https://docs.ansible.com/ansible/2.9/modules/list_of_files_modules.html)
* [Identity modules](https://docs.ansible.com/ansible/2.9/modules/list_of_identity_modules.html)
* [Inventory modules](https://docs.ansible.com/ansible/2.9/modules/list_of_inventory_modules.html)
* [Messaging modules](https://docs.ansible.com/ansible/2.9/modules/list_of_messaging_modules.html)
* [Monitoring modules](https://docs.ansible.com/ansible/2.9/modules/list_of_monitoring_modules.html)
* [Net Tools modules](https://docs.ansible.com/ansible/2.9/modules/list_of_net_tools_modules.html)
* [Network modules](https://docs.ansible.com/ansible/2.9/modules/list_of_network_modules.html)
* [Notification modules](https://docs.ansible.com/ansible/2.9/modules/list_of_notification_modules.html)
* [Packaging modules](https://docs.ansible.com/ansible/2.9/modules/list_of_packaging_modules.html)
* [Remote Management modules](https://docs.ansible.com/ansible/2.9/modules/list_of_remote_management_modules.html)
* [Source Control modules](https://docs.ansible.com/ansible/2.9/modules/list_of_source_control_modules.html)
* [Storage modules](https://docs.ansible.com/ansible/2.9/modules/list_of_storage_modules.html)
* [System modules](https://docs.ansible.com/ansible/2.9/modules/list_of_system_modules.html)
* [Utilities modules](https://docs.ansible.com/ansible/2.9/modules/list_of_utilities_modules.html)
* [Web Infrastructure modules](https://docs.ansible.com/ansible/2.9/modules/list_of_web_infrastructure_modules.html)
* [Windows modules](https://docs.ansible.com/ansible/2.9/modules/list_of_windows_modules.html)


## Roles

### o que são roles?

As roles (funções) são um conjunto de itens independentes destinados a provisionar uma determinada aplicação/infraestrutura...

As roles possuem uma estrutura padrão de diretórios para seus projetos:
```sh
playbook.yml
roles/
    common/
        tasks/
        handlers/
        files/
        templates/
        vars/
        defaults/
        meta/
```        

- Tasks: lista de tarefas a serem executadas em uma role.
- Handlers: são eventos acionados por uma task.
- Files: arquivos utilizados para deploy dentro de uma role.
- Templates: modelos para deploy dentro de uma role (Permite uso de variáveis.)
- Vars: variáveis adicionais de uma role.
- Defaults: variaveis padrão de uma role. Prioridade máxima.
- Meta: trata dependências de uma role por outra role.

## Playbooks
Permite gerenciar e orquestrar várias máquinas de forma mais simples do que quando utilizado a opção adhoc.
Adhoc é utilizado apenas para realizar tarefas simples.
[Link da documentação](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html)

```yaml
---
- name: Apache playbook
  hosts: apache
  roles:
    - apache  
    
...
```

## Lista de comandos: 
```bash
ansible-playbook --help

ansible-playbook -{opções} playbook-criada.yml

ansible-playbook -i hosts  playbook-criada.yml

ansible-playbook -i hosts  playbook-criada.yml --syntex-check #verifica sintaxe dos arquivos. Não executa.

ansible-playbook -i hosts  playbook-criada.yml --check #simula a execução da playbook mas sem realizar alterações.

ansible-playbook -i hosts playbook-criada.yml --limit host #ignora os hosts e roda apenas no definido pelo limit.

ansible-playbook -i hosts  playbook-criada.yml --forks=NUMERO #expecifica o número de processos a serem executados em paralelo.

ansible-playbook -i hosts  playbook-criada.yml --list-hosts #lista os hosts configurados.

ansible-playbook -i hosts  playbook-criada.yml --list-tasks #lista as tasks configuradas.

```

## Ansible Vault
Featue do ansible utilizado para criptografar arquivos com dados sensíveis.

```bash
ansible-vault create passwd.yml

ansible-vault edit passwd.yml

ansible-vault view passwd.yml

ansible-vault encrypt passwd.yml

ansible-vault decrypt passwd.yml
```