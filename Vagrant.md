# Vagrant

## O que é o Vagrant: 

É uma [ferramenta](https://www.vagrantup.com/docs/index) de linha de comando para criar e gerenciar o ciclo de vida de máquinas virtuais. 

### Vantagens:
    - Ela possui uma interface única de comunicação com os hipervisors (termo  genérico para descrever um virtualizador):
        - hyperv (microsoft)
        - virtualbox (oracle)
        - vmware
        - Docker, etc
    - Principio de infraestrutura como código (IaC): Através de um código conseguimos recriar uma infra.

## Como funciona?


![Vagrant schema](./vagrant-schema.png)

## Vagrantfile

Arquivo utilizado para descrever como configurar e provisionar uma máquina. Tais arquivos são escritos utilizando a linguagem Ruby. Apesar de serem escritos em ruby não precisamos ter conhecimento na sintaxe da linguagem para escrevermos os nossos próprios Vagrantfiles.
[Documentação Vagrantfiles](https://www.vagrantup.com/docs/vagrantfile)

O Vagrantfile possui 5 partes principais:
* config.vm.box: Sistema operacional.
* config.vm.provider: Provider: virtualbox, vmware, hiperv.
* config.vm.network: Como host enxerga o box.
* config.vm.synced_folder: Como vc acessará arquivos do host. 
* config.vm.provision: O que queremos configurar.

```ruby
Vagrant.configure("2") do |config|

  config.vm.box = "ubuntu/trusty64"

  config.vm.provider "virtualbox" do |vb|
  #   vb.gui = true
      vb.memory = 1024
      vb.cpus = 4
  end
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "private_network", ip: "192.168.33.10"

  config.vm.network "public_network"
  config.vm.synced_folder ".", "/var/www/html", :mount_options => ["dmode=777", "fmode=666"]

  config.vm.provision "shell", inline: <<-SHELL
     apt-get update
     apt-get install -y apache2
  SHELL
  
end

```

## Lista de comandos: 
```bash

vagrant --help
vagrant box list
vagrant autocomplete install --bash --zsh # autocomplete: apenas para bash zsh
vagrant init ubuntu/trusty64
vagrant up 
vagrant destroy
vagrant suspend
vagrant resume
vagrant reload (aumentar a memoria e realizar o reload)
vagrant halt
vagrant ssh
vagrant status
vagrant global-status

vagrant snapshot save [vm-name] NAME
```