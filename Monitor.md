# Monitoramento

Quando falamos de monitoramento em DevOps, nos referimos ao processo que garante uma evolução escalável da rotina produtiva. Ou seja, muito além de identificar os erros e falhas de forma instantânea, falamos de um monitoramento que ajuda a medir o cumprimento dos padrões de SLA.

O Prometheus é um ótimo exemplo dessa ferramenta. Ele cresceu rapidamente e se tornou a ferramenta principal para monitoramento e alerta de sistemas de containers. Sua principal força reside no monitoramento e armazenamento eficientes de métricas do lado do servidor. O Prometheus é totalmente open source e é simples e fácil de operar.

# Laboratório

Para este laboratório, criaremos a nossa própria stack de monitoramento.

Na figura abaixo temos um exemplo do que pretendemos obter ao final desse capítulo.

![monitor](./monitor.png)

## Descrição dos componentes

1. **Exporters**: Os exporters são uma camada intermediária entre o aplicações criadas para coletar e exportar métricas de aplicações.
1. **Prometheus**: Sistema utilizado para armazenar métricas de aplicações em um time series database.
1. **AlertManager**: Sistema que trabalha integrado com o Prometheus. Através dele, conseguimos criar regras de alertas e disparar notificações: por email, slack, teams, etc.
1. **Grafana**: Solução para observability. Com ele somos capazes de criar inúmeros dashboards para expor as métricas coletadas utilizando o prometheus.

# Prometheus:

Como citado anteriormente, o prometheus é uma aplicação que captura métricas e armazena os resultados em um banco time series.
Ele possui um arquivo de configuração em que indicamos a frequência de scrap e as regras de alertas.

![prometheus](./prometheus.png)

# Exemplo de rules:

Job missing:

```sh
  - alert: PrometheusJobMissing
    expr: absent(up{job="meu job"})
    for: 0m
    labels:
      severity: warning
    annotations:
      summary: Job missing (instance {{ $labels.instance }})
      description: "Meu Job has disappeared\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"

```

Alerta falhando:

```sh
  - alert: PrometheusAlertmanagerNotificationFailing
    expr: rate(alertmanager_notifications_failed_total[1m]) > 0
    for: 0m
    labels:
      severity: critical
    annotations:
      summary: Prometheus AlertManager notification failing (instance {{ $labels.instance }})
      description: "Alertmanager is failing sending notifications\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"

```
